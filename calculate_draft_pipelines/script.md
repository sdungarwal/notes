## Calculate Draft pipeline script

```ruby
READY_MSG = 'marked this merge request as **ready**'
DRAFT_MSG = 'marked this merge request as **draft**'

class ProgressTracker # Refer to https://gitlab.com/dgruzd/progress_tracker#script for the latest version
  def initialize(total_count, log_delay_seconds: 30, prefix: nil, logger: nil)
    @i = 0; @tc = total_count; @delay = log_delay_seconds.to_i; @mp = prefix; @l = logger; @last_log_at = Time.at(0)
  end

  def iteration(num = 1)
    @i += num; (@last_log_at = Time.now; p = "#{@i}/#{@tc}"; output = "#{@mp}#{p.ljust(@tc.digits.length * 2 + 1)} (#{format "%05.2f", (@i/@tc.to_f) * 100}%)"; (@l ? @l.info(output) : puts(output))) if Time.now - @last_log_at > @delay || @i == @tc
  end
end

def calculate_draft_ranges(merge_request)
  ready_timestamps = merge_request.notes.where(system: true, note: READY_MSG).find_each.map(&:created_at)
  draft_timestamps = merge_request.notes.where(system: true, note: DRAFT_MSG).find_each.map(&:created_at)

  timeline = [
    ready_timestamps.map{|n| { created_at: n.to_i, draft: false } },
    draft_timestamps.map{|n| { created_at: n.to_i, draft: true } }
  ].flatten.compact.sort_by{|h| h[:created_at] }

  return [] if timeline.blank?

  timeline.unshift({ created_at: 0, draft: !timeline.first[:draft]} )

  draft_ranges = []
  begins_at = nil
  timeline.each do |t|
    if t[:draft]
        begins_at ||= t[:created_at]
    elsif begins_at
        draft_ranges << (begins_at..t[:created_at])
        begins_at = nil
    end
  end
  draft_ranges << (begins_at..Time.now.utc.to_i) if begins_at

  draft_ranges
end

def count_draft_pipelines_for(merge_request)
  draft_ranges = calculate_draft_ranges(merge_request)

  return 0 if draft_ranges.blank?

  pipeline_timestamps = merge_request.all_pipelines.find_each.map(&:created_at).map(&:to_i)
  
  pipeline_timestamps.count do |timestamp|
    draft_ranges.any?{ |r| r.include?(timestamp) }
  end
end

def execute(project:, time_range:)
  scope = project.merge_requests.where(created_at: time_range)
  progress_tracker = ProgressTracker.new(scope.count)
  total_pipelines = 0

  scope.find_each do |mr|
    total_pipelines += count_draft_pipelines_for(mr)
    progress_tracker.iteration
  end

  total_pipelines
end

execute(project: Project.find(278964), time_range: (1.week.ago..))
```