## Schema design

### Model: `Search::Zoekt::EnabledNamespace`

Table name: `zoekt_enabled_namespaces`

| name                 | type        | comment                                                       |
| -------------------- | ----------- | ------------------------------------------------------------- |
| `id`                 | `bigint`    | `PRIMARY KEY`                                                 |
| `root_namespace_id`  | `bigint`    | `NOT NULL` , `FK REFERENCES namespaces(id) ON DELETE CASCADE` |
| `search`             | `bool`      | `NOT NULL` , `DEFAULT true` search enabled boolean flag       |
| `number_of_replicas` | `smallint`  | `NOT NULL`, `DEFAULT 1` (skip adding for now)                 |
| `created_at`         | `timestamp` | `NOT NULL`                                                    |
| `updated_at`         | `timestamp` | `NOT NULL`                                                    |

Add validation: `root_namespace_id` can only point to a root namespace

* Indices
    - `btree UNIQUE (root_namespace_id)`

### Model: `Search::Zoekt::Index`

Table name: `zoekt_indices`

| name                                  | type        | comment                                                         |
| ------------------------------------- | ----------- | --------------------------------------------------------------- |
| `id`                                  | `bigint`    | `PRIMARY KEY`                                                   |
| `zoekt_enabled_namespace_id`          | `bigint`    | `FK REFERENCES zoekt_enabled_namespaces(id) ON DELETE SET NULL` |
| `zoekt_node_id`                       | `bigint`    | `NOT NULL` , `FK REFERENCES zoekt_nodes(id) ON DELETE CASCADE`  |
| `state`                               | `smallint`  | `NOT NULL` , `DEFAULT 0`                                        |
| `created_at`                          | `timestamp` | `NOT NULL`                                                      |
| `updated_at`                          | `timestamp` | `NOT NULL`                                                      |
| `original_zoekt_enabled_namespace_id` | `bigint`    | `NOT NULL`                                                      |
| `metadata`                            | `jsonb`     | `NOT NULL` `DEFAULT '{}'::jsonb`                                |

Add uniqueness validation for `zoekt_enabled_namespace_id` and `zoekt_node_id`

* Indices
    - `btree UNIQUE (zoekt_enabled_namespace_id, zoekt_node_id)`
    - `btree (zoekt_node_id)`
    - `btree (state)`
* State Enum
    - 0: `pending`
    - 1: `initializing`
    - 10: `ready`
    - 20: `reallocating`
    - 30: `orphaned`
    - 40: `to_delete`
    - 50: `deleting`

Add a migration for existing records to set to :ready

### Model: `Search::Zoekt::Repository`

Table name: `zoekt_repositories`

| name                  | type        | comment                                                          |
| --------------------- | ----------- | ---------------------------------------------------------------- |
| `id`                  | `bigint`    | `PRIMARY KEY`                                                    |
| `zoekt_index_id`      | `bigint`    | `NOT NULL` , `FK REFERENCES zoekt_indices(id) ON DELETE CASCADE` |
| `project_id`          | `bigint`    | `FK REFERENCES projects(id) ON DELETE SET NULL`                  |
| `state`               | `smallint`  | `NOT NULL` , `DEFAULT 0`                                         |
| `size_bytes`          | `bigint`    |                                                                  |
| `index_file_count`    | `integer`   |                                                                  |
| `created_at`          | `timestamp` | `NOT NULL`                                                       |
| `updated_at`          | `timestamp` | `NOT NULL`                                                       |
| `indexed_at`          | `timestamp` |                                                                  |
| `original_project_id` | `bigint`    | `NOT NULL`                                                       |

Add uniqueness validation for `zoekt_index_id` and `project_id`

* Indices
    - `btree UNIQUE (zoekt_index_id, project_id)`
    - `btree (project_id)`
    - `btree (state)`
* State Enum
    - 0: `pending`
    - 1: `initializing`
    - 10: `ready`
    - 30: `orphaned`
    - 40: `to_delete`
    - 50: `deleting`

### Model: `Search::Zoekt::Task`

Table name: `zoekt_tasks`

List partition strategy (partition id) sliding list strategy

https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125333

| name                   | type        | comment                                                               |
| ---------------------- | ----------- | --------------------------------------------------------------------- |
| `id`                   | `bigint`    | `PRIMARY KEY`                                                         |
| `zoekt_node_id`        | `bigint`    | `NOT NULL` , `FK REFERENCES zoekt_nodes(id) ON DELETE CASCADE`        |
| `zoekt_repository_id`  | `bigint`    | `NOT NULL` , `FK REFERENCES zoekt_repositories(id)`  `DELETE CASCADE` |
| `state`                | `smallint`  | `NOT NULL` , `DEFAULT 0`                                              |
| `task_type`            | `smallint`  | `NOT NULL`                                                            |
| `retries_left`         | `smallint`  | `NOT NULL` , `DEFAULT 5`                                              |
| `serialized_arguments` | `text`      |                                                                       |
| `index_at`             | `timestamp` | `NOT NULL`, `DEFAULT CURRENT_TIMESTAMP()`                             |
| `updated_at`           | `timestamp` | `NOT NULL`                                                            |

* DB Constraints
    - state has to be :failed if `retries_left == 0`
* Indices
    - `btree (zoekt_node_id, index_at, state)`
    - `btree (index_at)`
    - `btree (zoekt_repository_id)`
* State Enum
    - 0: `pending`
    - 1: `done`
    - 255: `failed`
* Task Type Enum
    - 0: index
    - 1: merge
    - 2: delete
