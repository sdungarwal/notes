# Other

## Search

1. The `use_zoekt?` method should use state in order to decide if something is ready to be searched.
1. When we perform a search request, we should
  ```ruby
  scope = Search::Zoekt::EnabledNamespace.where(root_namespace_id: root_namespace_id, search: true)
  Search::Zoekt::Index.where(zoekt_enabled_namespace_id: scope.select(:id), state: [:ready, :reallocating]).pluck(:zoekt_node_id).sample
  ```

## Index

```ruby
scope = Search::Zoekt::EnabledNamespace.where(root_namespace_id: project.root_ancestor.id)
Search::Zoekt::Index.where(zoekt_enabled_namespace_id: scope.select(:id), state: [:initializing, :ready, :reallocating]).find_each do |index|
  repository = index.zoekt_repositories.find_or_create!(project_id: project.id)
  repository.tasks.create!(zoekt_node_id: index.zoekt_node_id, task_enum: :index)
end
```

## Delete

There's no really need for explicit deletions because Scheduling worker will mark the record 

```ruby
Search::Zoekt::Repository.where(project_id: project.id).update_all(state: :to_delete)
```

## Tasks API

1. Next task(s): `Search::Zoekt::Node.find_by(node_uuid).tasks.where(state: :pending).where('index_at < ?', Time.now).order(:index_at).limit(limit)`

1. We should return nothing if indexing is paused

#### Index

1. On success
  1. `task.zoekt_repository.update!(size_bytes: size_bytes, index_file_count: index_file_count, indexed_at: Time.now)`

#### Delete

1. On success
  1. `task.zoekt_repository.destroy!`

## Callback payloads

### Index

### Delete

### Merge